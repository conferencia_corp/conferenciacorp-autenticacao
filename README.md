# Autenticacao #


A Simple User with authentication with JWT

## Installation

```
#!shell

composer require conferenciacorp/autenticacao
```

## Usage

```php
<?php

use ConferenciaCorp\Autenticacao\Autenticacao;

//create the authentication
$autenticacao = new Autenticacao($secretKey);

//verify if the token is valid
if ($autenticacao->setToken($token)->isValid()) {

    //get user from the payload of the JWT
    $user = $autenticacao->getUser();

}
```
After that you have an object from `ConferenciaCorp\Autenticacao\User\User` and you can check even permissions
```php
<?php

$aclAllowed = 'developer';

if ($user->can($aclAllowed)) {
    echo "Hey, {$user->getNome()}. You are an Developer!";
}
```

## Payload - minimum requirements
```json
{
    "nome": "Marcelo Cerqueira",
    "acl": [
        "developer"
    ]
}
```

## Payload - additional data
if you want extra data on your payload:
```json
{
    "nome": "Marcelo Cerqueira",
    "acl": [
        "developer"
    ],
    "data": [
        {
            "company": "ConferenciaCorp"
        }
    ]
}
```

And in your php
```php
$data = $user->getData();

echo "Amazing! You work at {$data['company']}!";
```
Or
```php
echo "Amazing! You work at {$user->company}!";
```

## Credits

Thanks to [Namshi/Jose](https://github.com/namshi/jose) PHP JWT Library