<?php

namespace ConferenciaCorp\Autenticacao;

use \InvalidArgumentException;
use \Namshi\JOSE\SimpleJWS;
use \ConferenciaCorp\Autenticacao\User\User;

class Autenticacao
{
    private $secretKey;
    private $alg;

    private $token;

    public function __construct($secretKey, $alg = 'HS256')
    {
        $this->secretKey = $secretKey;
        $this->alg = $alg;
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function isValid()
    {
        if (!$this->hasToken()) {
            throw new InvalidArgumentException('Missing token', 401);
        }

        $jws = SimpleJWS::load($this->token);

        if (!$jws->isValid($this->secretKey, $this->alg)) {
            return false;
        }

        return true;
    }

    public function getUser()
    {
        if (!$this->hasToken()) {
            throw new InvalidArgumentException('Missing token', 401);
        }

        $jws = SimpleJWS::load($this->token);

        return new User($jws->getPayload());
    }

    private function hasToken()
    {
        return !empty($this->token);
    }
}
