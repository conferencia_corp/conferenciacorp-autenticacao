<?php

namespace ConferenciaCorp\Autenticacao\Token;

use Namshi\JOSE\SimpleJWS;

class Generator
{

    private static $payloadSchema = [
        'nome' => '',
        'acl'  => [],
        'data' => []
    ];

    private $jws;

    private $key;
    private $alg;
    private $expiration;

    private $payload;

    public function __construct($key, $alg = 'HS256', $expiration = 5)
    {
        $this->key = $key;
        $this->alg = $alg;
        $this->expiration = $expiration;
        $this->jws = new SimpleJWS(['alg' => $this->alg]);

        $this->payload = self::$payloadSchema;
    }

    public function setPayload(array $payload)
    {
        self::validatePayload($payload);

        $this->payload = array_merge(self::$payloadSchema, $payload);
    }

    public function setName($name)
    {
        $this->payload['nome'] = $name;

        return $this;
    }

    public function setAcl(array $acl)
    {
        $this->payload['acl'] = $acl;

        return $this;
    }

    public function appendAcl($acl)
    {
        $this->payload['acl'][] = $acl;

        return $this;
    }

    public function setData(array $data)
    {
        $this->payload['data'] = $data;

        return $this;
    }

    public function appendData($key, $value)
    {
        $this->payload['data'][$key] = $value;

        return $this;
    }

    public function setExpiration($minutes)
    {
        $this->expiration = $minutes;

        return $this;
    }

    public function getToken()
    {
        if (is_int($this->expiration)) {
            $this->payload['exp'] = $this->getExpiration();
        }

        $this->jws->setPayload($this->payload);

        $this->jws->sign($this->key);

        return $this->jws->getTokenString();
    }

    private function getExpiration()
    {
        $date = new \DateTime("now");
        $date->modify("+{$this->expiration} minutes");

        return $date->format('U');
    }

    public static function validatePayload(array $payload)
    {
        foreach (self::$payloadSchema as $key => $value) {
            if (!isset($payload[$key])) {
                continue;
            }

            $type = gettype($value);
            if ($type != gettype($payload[$key])) {
                throw new \Exception("O campo {$key} deve ser do tipo {$type}");
            }
        }

        return true;
    }
}