<?php

namespace ConferenciaCorp\Autenticacao\Token;

class Factory
{
    private $alg;
    private $expiration;
    private $defaultPayload = null;

    public function __construct($alg = 'HS256', $expiration = 5)
    {
        $this->alg = $alg;
        $this->expiration = $expiration;
    }

    public function setDefaultPayload(array $payload)
    {
        Generator::validatePayload($payload);

        $this->defaultPayload = $payload;
    }

    public function create($key)
    {
        $generator = new Generator($key, $this->alg, $this->expiration);

        if ($this->defaultPayload !== null) {
            $generator->setPayload($this->defaultPayload);
        }

        return $generator;
    }
}