<?php

namespace ConferenciaCorp\Autenticacao\Builder;

use ConferenciaCorp\Autenticacao\Token\Generator;

class TokenBuilder
{
    private $generator;

    public function __construct($secretKey, $alg)
    {
        $this->generator = new Generator($secretKey, $alg, null);

        $this->generator->setName('Marcelo');
        $this->generator->appendAcl('developer');
        $this->generator->appendData('company', 'ConferenciaCorp');
    }

    public function get()
    {
        return $this->generator->getToken();
    }

    public function getWithExpiryDate()
    {
        $this->generator->setExpiration(5);
        return $this->generator->getToken();
    }

    public function getTokenExpired()
    {
        $this->generator->setExpiration(-5);
        return $this->generator->getToken();
    }
}